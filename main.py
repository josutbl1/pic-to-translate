import os
from PIL import ImageGrab, Image
import pyocr
import pyocr.builders
import json
import webbrowser
from datetime import datetime as dt
import pyperclip

def log_error(err):
    print("There was an error, please refer to log.txt\n")
    with open("log.txt", "at") as lf:
        now = dt.now()
        lf.write(now.strftime("%d")+" "+now.strftime("%b")+" "+now.strftime("%y")+" "+now.strftime("%I")+":"+now.strftime("%M"))
        lf.write("\n")
        lf.write("Error: "+err+"\n\n")
        lf.close()
    quit()

image = ImageGrab.grabclipboard()
if not isinstance(image, Image.Image):
    log_error("Clipboard data wasn't an image. Be sure to take the screenshot 'Windows Key+Shift+S' before running the program.")

path_tesseract = "C:\\Program Files (x86)\\Tesseract-OCR"
if path_tesseract not in os.environ["PATH"].split(os.pathsep):
    os.environ["PATH"] += os.pathsep + path_tesseract

tools = pyocr.get_available_tools()
for i in range(0, len(tools)):
    if tools[i].get_name().__contains__("Tesseract"):
        tool = tools[i]
        break
    if i == len(tools)-1:
        log_error("Tesseract not installed.")

builder = pyocr.builders.TextBuilder()
result = tool.image_to_string(image, lang="jpn", builder=builder)
if result == "":
    log_error("Text wasn't found.")

result = result.replace("\n", "")
pyperclip.copy(result)

f = open("settings.json", "rt")
settings = json.loads(f.read())
f.close()

f = open ("language.json", "rt")
codes = json.loads(f.read())
f.close()

for i in range(0, len(codes)):
    if codes[i]["name"].__contains__(settings["translate-to"]):
        langcode = codes[i]["code"]

webbrowser.open("https://translate.google.com/?tl="+ langcode +"&text="+result, new=2, autoraise=True)