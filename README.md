# pic-to-translate

## Welcome

This is a program that lets you "screengrab" a picture with Japanese text and translate it to any language you want.

## Installation

You will need:
* Windows computer, only tested on Windows 11 but should work on Windows 10.
* Latest version of Python, download Python from [here](https://www.python.org/downloads/). Make sure to [add Python on PATH!](https://www.geeksforgeeks.org/how-to-add-python-to-windows-path/)
* Download and install [Tesseract](https://github.com/UB-Mannheim/tesseract/wiki). You will need to install Japanese language pack, you can follow instructions from [here](https://gammasoft.jp/blog/tesseract-ocr-install-on-windows/). Adding Tesseract to PATH is not required.
* Install dependencies. Open the command prompt and enter the following commands:
```
pip install pillow
pip install pyocr
pip install pyperclip
```

## How to use

* Download the files, open the command prompt on the folder where the files are.
* Press the **Windows Key + Shift + S** and select the space where the Japanese text is on the screen.
* On the command prompt and execute the following command `python main.py`
* Google Translator should open on the web browser with the Japanese text taken from the picture.
* It also copies the text to the clipboard.
* It also should recognize English and the default language of the device.

## Change languages

* Open `settings.json` on a text editor and change `translate-to` field to any other language.
* Refer to [this](https://cloud.google.com/translate/docs/languages) webpage for correct language names. Some notes: Caps-sensible. For simplified Chinese use `Chinese`, there is no traditional support at the moment.

## Errors that can happen

* The installation process can be hard and missing something is easy.
* The character recognition software is not perfect and some text can't be recognized.
* If the program doesn't work as intended please go to the program folder and open `log.txt` where the erro whould be written there.
* If you have any question or help with the installation please contact me by mail at `josuferrer@outlook.com`

## What's to come?

* Easier installation, and usage
* Better character recognition
* Other functions

## Dependencies

* Tesseract is the OCR engine used for this project, I haven't tested others
* `pillow` for grabbing the image from the clipboard
* `pyocr` as a bridge from Python to Tesseract
* `pyperclip` to copy the recognized text to the clipboard
* `language.json` taken from [this](https://gist.github.com/jrnk/8eb57b065ea0b098d571#file-iso-639-1-language-json) repository

## Thank you

Thank you for using this program.
